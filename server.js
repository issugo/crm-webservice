// prérequis
const express = require('express')
const bodyParser = require('body-parser')
const http = require("http")
const cors = require('cors')
const OauthController = require('./controllers/oauth.controller');
const OAuthClientsModel = require('./models').oauthclient;
const OAuthServer = require('express-oauth-server');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
const LOGGER = require('./config/winston.config')("logger");
const fs = require('fs')

const neededFolders = ["logs", "public", "public/images", "public/logos"];
neededFolders.forEach(folder => {
    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);

        LOGGER.info(`${folder} created`);
    }
})

//variables nécessaire
const app = express()
const port = 3000

// chargement des models
const db = require("./models");
const res = require('express/lib/response');

// synchronization de la base
db.sequelize.sync()
    .then(() => {
        console.log('successfully sync base')
        LOGGER.info('successfully sync base')
        const firstOauthClient = {
            clientId: "express-client",
	        clientSecret: "express-secret",
	        redirectUris: "http://localhost:3000/oauth/callback",
	        grants: ["password", "refresh_token"]
        }
        OAuthClientsModel.count()
            .then(count => {
                if(count === 0) {
                    OauthController.setClient(firstOauthClient)
                        .then((client) => LOGGER.info(`INFO - first client created : ${client}`))
                        .catch((error) => {
                            LOGGER.error(error);
                        });
                }
            })
            .catch(error => {
                LOGGER.error(error);
            });

        const ADMINISTRATEUR = require("./models").administrateur;
        ADMINISTRATEUR.count()
            .then(async (nbAdministrateur) => {
                if (nbAdministrateur === 0) {
                    const saltRounds = 10;
                    const token = crypto.randomUUID();
                    const hashedToken = await bcrypt.hash(token, saltRounds);
                    ADMINISTRATEUR.create({
                        nom: "admin",
                        prenom: "admin",
                        email: "admin@admin.com",
                        password: hashedToken
                    })
                        .then((administrateur) => {
                            console.log(`INFO - admin ${administrateur.id} créé, api-token : ${token}`)
                            LOGGER.info(`INFO - admin ${administrateur.id} créé, api-token : ${token}`)
                        })
                }
            })
    })

// enabling cors request
app.use(cors())
// parse application/json
app.use(bodyParser.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))
//cacheMiddleware.attach(app)

app.oauth = new OAuthServer({
    model: OauthController,
    accessTokenLifetime: 10 * 60
});

// appel des routes
require('./routes/auth.routes')(app)
require('./routes/administrateur.routes')(app)
require('./routes/organisation.routes')(app)
require('./routes/client.routes')(app)
require('./routes/rdv.routes')(app)
require('./routes/log.routes')(app)


//default routes de test
app.get("/", function (req, res) {
    res.json("hello world")
})

//lancement serveur
http.createServer(app).listen(process.env.PORT || port)
