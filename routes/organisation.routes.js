const { authJwt, verifySignUp, logs } = require("../middleware");
const organisationController = require("../controllers/organisation.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    app.get("/organisations", [app.oauth.authenticate()], organisationController.getAll);
    
    app.get("/organisations/:id", [app.oauth.authenticate()], organisationController.getById);
    
    app.post("/organisations/create",  [app.oauth.authenticate()], organisationController.create);
    
    app.put("/organisations/:id", [app.oauth.authenticate()], organisationController.update);
    
    app.delete("/organisations/:id", [app.oauth.authenticate()], organisationController.delete)
};
