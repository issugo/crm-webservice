const { verifySignUp } = require("../middleware");
const OauthController = require('../controllers/oauth.controller');

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post('/oauth/token', app.oauth.token());

  app.post('/oauth/set_client', function (req, res, next) {
    OauthController.setClient(req.body)
        .then((client) => res.json(client))
        .catch((error) => {
            return next(error);
        });
  });

  app.get('/secret', app.oauth.authenticate(), function (req, res) {
    //console.log(res.locals.oauth.token.userId);
    res.json('secret area');
  });
};
