const { authJwt, verifySignUp, logs } = require("../middleware");
const clientController = require("../controllers/client.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    app.get("/clients", [app.oauth.authenticate()], clientController.getAll);
    
    app.get("/clients/:id", [app.oauth.authenticate()], clientController.getById);

    app.get("/clients/:id/rdvs", [app.oauth.authenticate()], clientController.getRdvByClient);
    
    app.post("/clients/create",  [app.oauth.authenticate()], clientController.create);
    
    app.put("/clients/:id", [app.oauth.authenticate()], clientController.update);
    
    app.delete("/clients/:id", [app.oauth.authenticate()], clientController.delete)
};
