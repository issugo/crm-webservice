const { rateLimiter } = require("../middleware");
const administrateurController = require("../controllers/administrateur.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/administrateurs", [app.oauth.authenticate()], administrateurController.getAll);
    
    app.get("/administrateurs/:id", [app.oauth.authenticate()], administrateurController.getById);
    
    app.use('/administrateurs/create', rateLimiter);

    app.post("/administrateurs/create", [app.oauth.authenticate()], administrateurController.create);
    
    app.put("/administrateurs/:id", [app.oauth.authenticate()], administrateurController.update);
    
    app.delete("/administrateurs/:id", [app.oauth.authenticate()], administrateurController.delete);
};