const { authJwt, verifySignUp, logs } = require("../middleware");
const photoController = require("../controllers/photo.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    app.get("/photo", [app.oauth.authenticate()], photoController.getAll);
    
    app.get("/photo/:id", [app.oauth.authenticate()], photoController.getById);
    
    app.post("/photo/create",  [app.oauth.authenticate(),], photoController.create);
    
    app.put("/photo/:id", [app.oauth.authenticate()], photoController.update);
    
    app.delete("/photo/:id", [app.oauth.authenticate()], photoController.delete)

    app.get("/photo/show/:imageName", [app.oauth.authenticate()], photoController.show);
};
