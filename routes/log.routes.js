const logController = require("../controllers/log.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    app.get("/logs", [app.oauth.authenticate()], logController.getDefault);

    app.get("/logs/info", [app.oauth.authenticate()], logController.getInfo);

    app.get("/logs/error", [app.oauth.authenticate()], logController.getError);

    app.get("/logs/sql", [app.oauth.authenticate()], logController.getSql);
};
