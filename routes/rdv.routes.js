const { authJwt, verifySignUp, logs } = require("../middleware");
const rdvController = require("../controllers/rdv.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    app.get("/rdvs", [app.oauth.authenticate()], rdvController.getAll);
    
    app.get("/rdvs/:id", [app.oauth.authenticate()], rdvController.getById);
    
    app.post("/rdvs/create",  [app.oauth.authenticate()], rdvController.create);
    
    app.put("/rdvs/:id", [app.oauth.authenticate()], rdvController.update);
    
    app.delete("/rdvs/:id", [app.oauth.authenticate()], rdvController.delete)
};
