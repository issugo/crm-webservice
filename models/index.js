const config = require("../config/db.config.js");
const LOGGER = require('../config/winston.config')("sequelize");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD, {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: false,
        logging: (str) => {LOGGER.sql(str)},
        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.administrateur = require("./administrateur.model")(sequelize, Sequelize);
db.organisation = require("./organisation.model")(sequelize, Sequelize);
db.client = require("./client.model")(sequelize, Sequelize);
db.photo = require("./photo.model.js")(sequelize, Sequelize);
db.rdv = require("./rdv.model")(sequelize, Sequelize);
db.oauthtoken = require("./oauthtoken.model")(sequelize, Sequelize);
db.oauthclient = require("./oauthclient.model")(sequelize, Sequelize);

db.administrateur.hasMany(db.organisation, {as: "organisations", foreignKey: "administrateur", onDelete: 'cascade', hooks: true });
db.organisation.belongsTo(db.administrateur, {
    foreignKey: "administrateur",
    as: "administrateurs",
    otherKey: "id"
})

db.organisation.hasMany(db.client, {as: "clients", foreignKey: "organisation", onDelete: 'cascade', hooks: true});
db.client.belongsTo(db.organisation, {
    foreignKey: "organisation",
    as: "organisations",
    otherKey: "id"
})

db.client.hasMany(db.photo, {as: "photos", foreignKey: "client", onDelete: 'cascade', hooks: true});
db.photo.belongsTo(db.client, {
    foreignKey: "client",
    as: "clients",
    otherKey: "id"
})

db.client.hasMany(db.rdv, {as: "rdvs", foreignKey: "client",  onDelete: 'cascade', hooks: true});
db.rdv.belongsTo(db.client, {
    foreignKey: "client",
    as: "clients",
    otherKey: "id"
})

db.administrateur.hasOne(db.oauthtoken, {as: "token", foreignKey: "userId",  onDelete: 'cascade', hooks: true});
db.oauthtoken.belongsTo(db.administrateur, {
    foreignKey: "userId",
    as: "user",
    otherKey: "id"
})

db.oauthclient.hasOne(db.oauthtoken, {as: "token", foreignKey: "clientId"})
db.oauthtoken.belongsTo(db.oauthclient, {
    foreignKey: "clientId",
    as: "client",
    otherKey: "id"
})

module.exports = db;
