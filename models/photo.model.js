module.exports = (sequelize, Sequelize) => {
    const Photo = sequelize.define("photos", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Path: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        date_envoi: {
            type: Sequelize.DATE,
            allowNull: true
        }
    });

    return Photo;
};