module.exports = (sequelize, Sequelize) => {
    const Administrateur = sequelize.define("administrateurs", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        prenom: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                is: /^[A-Za-z]*$/i
            }
        },
        nom: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                is: /^[A-Za-z]*$/i
            }
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                is: /^[A-Za-z+\.0-9]*@[A-Za-z+\.0-9]*$/i
            }
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return Administrateur;
};
