module.exports = (sequelize, Sequelize) => {
    const OauthClient = sequelize.define("oauthclients", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        clientId: {
            type: Sequelize.STRING
        },
        clientSecret: {
            type: Sequelize.STRING
        },
        redirectUris: {
            type: Sequelize.STRING
        },
        grants: {
            type: Sequelize.JSON
        }
    })

    return OauthClient;
}