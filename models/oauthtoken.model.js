module.exports = (sequelize, Sequelize) => {
    const OauthToken = sequelize.define("oauthtokens", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        accessToken: {
            type: Sequelize.STRING,
            allowNull: false
        },
        accessTokenExpiresAt: {
            type: Sequelize.DATE
        },
        refreshToken: {
            type: Sequelize.STRING
        },
        refreshTokenExpiresAt: {
            type: Sequelize.DATE
        }
    })

    return OauthToken;
}