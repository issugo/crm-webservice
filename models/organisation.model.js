module.exports = (sequelize, Sequelize) => {
    const Organisation = sequelize.define("organisations", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nom: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                is: /^[A-Za-z0-9]*$/i
            },
            unique: true
        },
        adresse: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        nb_salarie: {
            type: Sequelize.INTEGER,
            allowNull: true,
        },
        logo: {
            type: Sequelize.STRING,
            allowNull: true,
        }
    });

    return Organisation;
};
