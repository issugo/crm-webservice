module.exports = (sequelize, Sequelize) => {
    const Client = sequelize.define("clients", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        prenom: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                is: /^[A-Za-z]*$/i
            }
        },
        nom: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                is: /^[A-Za-z]*$/i
            }
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                is: /^[a-zA-Z0-9\.\-]+@[a-zA-Z0-9\.\-]+.[a-zA-Z]+$/i
            }
        },
        telephone: {
            type: Sequelize.STRING,
            allowNull: true,
            validate: {
                is: /^[0-9]*$/i
            }
        },
        age: {
            type: Sequelize.INTEGER,
            allowNull: true,
        }
    });

    return Client;
};
