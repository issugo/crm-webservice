module.exports = (sequelize, Sequelize) => {
    const Rdv = sequelize.define("rdvs", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        resume: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        lieu: {
            type: Sequelize.STRING,
            allowNull: true
        }
    });

    return Rdv;
};
