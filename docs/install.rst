CRM webservice Installation
===========================

Afin d'utiliser le webservice, il faut effectuer les simples étapes d'installation suivantes :

Prerequis
---------

Comme l'application est hébergée localement, il faudra une base de données MariaDb (notamment avec WAMP ou LAMP).

Le nom de la base de données importe peu puisque ces informations seront nécessaires lors de la configuration du .env

Installation
------------

Après avoir créer votre base de données MariaDb, il faut rentrer les informations de celle-ci dans le fichier ``.env.sample``::

    # jwt config
    SECRETKEY=

    # db config
    DB_HOST=
    DB_USER=
    DB_PASSWORD=
    DB_DB=
    DB_DIALECT=

    # mail config
    MAIL_USER=
    MAIL_PASS=

La ``SECRETKEY`` est nécessaire pour la configuration du jwt et on peut entrer n'importe quelle clé.  
Les autres informations sont les données pour accèder à votre base de données avec ``DB_DIALECT=mariadb``  
Pour l'envoi de mail lors de la création d'un administrateur, il faudra avoir rempli les informations de votre compte mail. Pensez à activer le SMTP authentifié sur le compte mail utilisé.  

On enlève le ``.sample`` du fichier.

Enfin on execute les commandes suivantes sur un terminal : ``npm install`` puis ``npm start``

Et voilà ! On est prêt à utiliser le webservice.