.. crm-webservice documentation master file, created by
   sphinx-quickstart on Thu Mar 31 09:58:55 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to crm-webservice's documentation!
==========================================

.. toctree::
   :maxdepth: 3

   intro
   install
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
