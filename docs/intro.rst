CRM webservice Legauf
=====================

Bienvenue sur la documentation du webservice réalisé par l'équipe Legauf

De quoi s'agit-il ?
-------------------

Dans un cadre académique, ce projet présente un webservice REST : un CRM

Cette documentation présente essentiellement les étapes d'installation nécessaire à son utilisation
et les requêtes disponibles sur une documentation swagger.