CRM webservice API
==================

"""
Utilisation
-------

Lors du premier lancement de l'API, un client OAUTH et un premier administrateur vont être créés. Les codes seront alors disponible dans les logs (console ou fichiers de logs).
pour obtenir un token à utiliser pour faire fonctionner l'API, il faut alors utiliser la route : 

    /oauth/token

    body x-www-form-urlencoded:
        username: admin@admin.com
        password: {api-token} #donné au lancement de l'API
        grant_type: password

    authentification Basic:
        username: express-client
        password: express-secret
    
    header :
        Content-Type: application/x-www-form-urlencoded

Pour utiliser le token sur le reste des routes, il faut ajouter le header : Authorization : Bearer {token}

"""

Swagger
-------

L'ensemble de la documentation API est disponible dans la rubrique /doc de l'api.

Pour y accéder, on doit d'abord effectuer l'installation puis entrer une commande tel que ::

    http://localhost:3000/doc

*Ces commandes peuvent être executées sur des client API comme ``Insomnia`` & ``Postman``*