//dépendances
const db = require("../models")
const ADMINISTRATEUR = db.administrateur;
const ORGANISATION = db.organisation;
const CLIENT = db.client;
const RDV = db.rdv;
const fs = require('fs');
const LOGGER = require('../config/winston.config')("organisation");

//fonctions pour les organisations
exports.getAll = async (req, res) => {
    // #swagger.tags = ['Organisation']
    LOGGER.info(`INFO - toute les organisations demandés par ${res.locals.oauth.token.userId}`);
    ORGANISATION.count({
        include: {
            model: ADMINISTRATEUR,
            as: 'administrateurs',
            where: {
                id: res.locals.oauth.token.userId
            }
        }
    }).then(async (nbOrganisation) => {
        let page = parseInt(req.query.page) - 1
        if (isNaN(page)) {
            page = 0
        }
        let organisations = await ORGANISATION.findAll({
            include: {
                model: ADMINISTRATEUR,
                as: 'administrateurs',
                where: {
                    id: res.locals.oauth.token.userId
                }
            },
            limit: 5,
            offset: 5 * page
        });
        let tempResult = []
        for(let i=0; i<organisations.length; i++) {
            let temp = JSON.parse(JSON.stringify(organisations[i]))
            try {
                delete temp.administrateurs
            } catch(e) {}
            tempResult.push(temp)
        }
        res.status(200).json({organisations: tempResult, pageTotale: nbOrganisation})
    })
}

exports.getById = async (req, res) => {
    // #swagger.tags = ['Organisation']
    let id = req.params.id;
    LOGGER.info(`INFO - information sur l'organisation ${id} demandé par admin ${res.locals.oauth.token.userId}`)
    if(isNaN(parseInt(id))) {
        res.status(400).json({message: "invalid id type"})
    }
    let organisation = await ORGANISATION.findOne({
        where: {
            id: id
        },
        include: {
            model: ADMINISTRATEUR,
            as: 'administrateurs',
            where: {
                id: res.locals.oauth.token.userId
            }
        }
    })
    if (client === null) {
        res.status(404).json({message: "client not found"})
    }
    organisation = JSON.parse(JSON.stringify(organisation))
    try {
        delete organisation.administrateurs
    } catch(error) {}
    res.status(200).json(organisation)
}

exports.create = async (req, res) => {
    // #swagger.tags = ['Organisation']
    const {
        nom,
        adresse,
        nb_salarie,
        logo
    } = req.body;
    LOGGER.info(`INFO - création d'organisation demandé par admin ${res.locals.oauth.token.userId} avec body : ${JSON.stringify({nom,prenom,email,telephone,age,organisation})}`)
    await ORGANISATION.create({
            nom: nom,
            adresse: adresse,
            nb_salarie: nb_salarie,
            logo: `/public/logos/${nom}.png`,
            administrateur: res.locals.oauth.token.userId
        })
        .then((organisation) => {
            LOGGER.info(`INFO - organisation ${organisation.id} créé`)
            let logo2 = logo.split(',')[1]
            fs.writeFile(`./public/logos/${organisation.nom}.png`, logo2, 'base64', function(err) {
                LOGGER.error(err)
              });
            res.json({
                organisation,
                msg: 'organisation créé correctement'
            })
        })
        .catch((error) => {
            LOGGER.error(error)
            if (error.errors[0].type == "unique violation") {
                res.status(400).json({message: "nom is not unique"})
            } else {
                res.status(400).json({message: error.errors[0].message})
            }
            
        });
}

exports.update = async (req, res) => {
    // #swagger.tags = ['Organisation']
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }

    let organisation = await ORGANISATION.findOne({
        where: {
            id: id
        },
        include: {
            model: ADMINISTRATEUR,
            as: 'administrateurs',
            where: {
                id: res.locals.oauth.token.userId
            }
        }
    })
    if (organisation === null) {
        res.status(404).json({message: "organisation not found"})
    } else {
        if (organisation.administrateur.id !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur de l'organisation"})
        } else {
            await ORGANISATION.update(
                req.body, {
                    where: {
                        id: req.params.id
                    }
                }).then(organisation => res.json({
                organisation,
                msg: 'organisation mis à jour'
            }))
            .catch(res.status(500).send({
                message: "error with the json data"
            }))
        }
    }
}

exports.delete = async (req, res) => {
    // #swagger.tags = ['Organisation']
    let organisation = await ORGANISATION.findOne({
        where: {
            id: id
        },
        include: {
            model: ADMINISTRATEUR,
            as: 'administrateurs',
            where: {
                id: res.locals.oauth.token.userId
            }
        }
    })
    if (organisation === null) {
        res.status(404).json({message: "organisation not found"})
    } else {
        if (organisation.administrateur.id !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur de l'organisation"})
        } else {
            await db.role.destroy({
                where: {
                    id: req.params.id
                }
            })
                .then(() => {res.status(200).json({message: "organisation supprimé avec succès"})})
                .catch((error) => {LOGGER.error(error); res.status(500).json(error.errors[0].message)})
        }
    }
}   
