//dépendences
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

//chargement des modèles
const db = require("../models");
//chargement des configs
const config = require("../config/auth.config");
const ADMINISTRATEUR = db.administrateur;
const Op = db.Sequelize.Op;

//module d'auth
exports.signin = (req, res) => {
  // #swagger.tags = ['Authentification']
  console.log(req.body)
  if (req.body.email === null || req.body.email === undefined){
    res.status(401).json({message: "email is empty"});
  }
  ADMINISTRATEUR.findOne({
    where: {
      email: req.body.email
    }
  })
    .then(administrateur => {
      if (!administrateur) {
        return res.status(404).send({ message: "Administrateur Not found." });
      }

      /*let passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }*/

      let token = jwt.sign({ id:administrateur.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

        res.status(200).send({
          id: administrateur.id,
          email: administrateur.email,
          accessToken: token
        });
      })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};