//dépendances
const RDV = require("../models").rdv;
const CLIENT = require("../models").client;
const ORGANISATION = require("../models").organisation;
const ADMINISTRATEUR = require("../models").administrateur;
const LOGGER = require('../config/winston.config')("rdv");

//fonctions pour les rdvs
exports.getAll = async (req, res) => {
    // #swagger.tags = ['RDV']
    LOGGER.info(`INFO - tout les rdv demandés par ${res.locals.oauth.token.userId}`);
    RDV.count({
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
                
            }
        },
    }).then(async (nbRDV) => {
        let page = parseInt(req.query.page) - 1
        if (isNaN(page)) {
            page = 0
        }
        let rdvs = await RDV.findAll({
            include: {
                model: CLIENT,
                as: "clients",
                include: {
                    model: ORGANISATION,
                    as: "organisations",
                    include: {
                        model: ADMINISTRATEUR,
                        as: 'administrateurs',
                        where: {
                            id: res.locals.oauth.token.userId
                        }
                    }
                    
                }
            },
            limit: 5,
            offset: 5 * page
        });
        let tempResult = []
        for(let i=0; i<rdvs.length; i++) {
            let temp = JSON.parse(JSON.stringify(rdvs[i]))
            try {
                delete temp.clients
            } catch(e) {}
            tempResult.push(temp)
        }
        res.status(200).json({rdvs: tempResult, pageTotale: nbRDV})
    })
}

exports.getById = async (req, res) => {
    // #swagger.tags = ['RDV']
    let id = req.params.id;
    LOGGER.info(`INFO - information sur le rdv ${id} demandé par admin ${res.locals.oauth.token.userId}`)
    if(isNaN(parseInt(id))) {
        res.status(400).json({message: "invalid id type"})
    }
    let rdv = await RDV.findOne({
        where: {
            id: id
        },
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
                
            }
            
        }
    })
    if (rdv === null) {
        res.status(404).json({message: "rdv not found"})
    } else {
        rdv = JSON.parse(JSON.stringify(rdv))
        try {
            delete rdv.clients
        } catch(error) {}
        res.status(200).json(rdv)
    }   
}

exports.create = async (req, res) => {
    // #swagger.tags = ['RDV']
    const {
        resume,
        date,
        lieu,
        client
    } = req.body;
    LOGGER.info(`INFO - création de rdv demandé par admin ${res.locals.oauth.token.userId} avec body : ${JSON.stringify({resume, date, lieu, client})}`)
    if (client === undefined) {
        res.status(400).json({message: "client not precised"})
    }
    await CLIENT.findOne({
        where: {
            id: client
        },
        include: {
            model: ORGANISATION,
            as: "organisations",
            include: {
                model: ADMINISTRATEUR,
                as: 'administrateurs',
                where: {
                    id: res.locals.oauth.token.userId
                }
            }
            
        }
    }).then(async (client) => {
        if (client === null) {
            res.status(404).json({message: "client not found"})
        } else {
            if (client.organisations.administrateur !== res.locals.oauth.token.userId) {
                res.status(403).json({message: "vous n'êtes pas l'administrateur du client"})
            } else {
                await RDV.create({
                    resume: resume,
                    date: date,
                    lieu: lieu || null
                })
                .then((rdv) => {
                    LOGGER.info(`INFO - rdv ${rdv.id} créé`)
                    res.json({
                        rdv,
                        msg: 'rdv créé correctement'
                    })
                })
                .catch((error) => {
                    LOGGER.error(error)
                    res.status(400).json({message: error.errors[0].message})
                });
            }
        }
    })
    .catch((error) => {res.status(500).json(error.message)})
}

exports.update = async (req, res) => {
    // #swagger.tags = ['RDV']
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }
    LOGGER.info(`INFO - modification du rdv ${req.params.id} demandé par admin ${res.locals.oauth.token.userId}`)
    let rdv = await RDV.findOne({
        where: {
            id: id
        },
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
            }
        }
    })
    if (rdv === null) {
        res.status(404).json({message: "rdv not found"})
    } else {
        if (rdv.clients.organisations.administrateur !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur du rdv"})
        } else {
            await RDV.update(
                req.body, {
                    where: {
                        id: req.params.id
                    }
                }).then((rdv) => {
                    LOGGER.info(`INFO - modification du rdv ${req.params.id} effectué par admin ${res.locals.oauth.token.userId}`)
                    res.json({
                        rdv,
                        msg: 'rdv mis à jour'
                    })
                })
            .catch(res.status(500).send({
                message: "error with the json data"
            }))
        }
    }
}

exports.delete = async (req, res) => {
    // #swagger.tags = ['RDV']
    LOGGER.info(`INFO - suppression du rdv ${req.params.id} demandé par admin ${res.locals.oauth.token.userId}`)
    let rdv = await RDV.findOne({
        where: {
            id: id
        },
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
            }
        }
    })
    if (rdv === undefined) {
        res.status(404).json({message: "rdv not found"})
    } else {
        if (rdv.clients.organisations.administrateur !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur du rdv"})
        } else {
            await RDV.destroy({
                where: {
                    id: req.params.id
                }
            })
                .then(() => {
                    LOGGER.info(`INFO - suppression du rdv ${req.params.id} effectué par admin ${res.locals.oauth.token.userId}`)
                    res.status(200).json({message: "rdv supprimé avec succès"})
                })
                .catch((error) => {LOGGER.error(error); res.status(500).json(error.errors[0].message)})
        }
    }
}
