//dépendances
const CLIENT = require("../models").client;
const ORGANISATION = require("../models").organisation;
const ADMINISTRATEUR = require("../models").administrateur;
const RDV = require("../models").rdv;
const LOGGER = require('../config/winston.config')("client");

//fonctions pour les clients
exports.getAll = async (req, res) => {
    // #swagger.tags = ['Client']
    LOGGER.info(`INFO - tout les clients demandés par ${res.locals.oauth.token.userId}`);
    CLIENT.count({
        include: {
            model: ORGANISATION,
            as: "organisations",
            include: {
                model: ADMINISTRATEUR,
                as: 'administrateurs',
                where: {
                    id: res.locals.oauth.token.userId
                }
            }
        }
    }).then(async (nbClient) => {
        let page = parseInt(req.query.page) - 1
        if (isNaN(page)) {
            page = 0
        }
        let clients = await CLIENT.findAll({
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
            },
            limit: 5,
            offset: 5 * page
        });
        let tempResult = []
        for(let i=0; i<clients.length; i++) {
            let temp = JSON.parse(JSON.stringify(clients[i]))
            try {
                delete temp.organisations
            } catch(e) {}
            tempResult.push(temp)
        }
        res.status(200).json({clients: tempResult, pageTotale: nbClient})
    })
}

exports.getById = async (req, res) => {
    // #swagger.tags = ['Client']
    let id = req.params.id;
    LOGGER.info(`INFO - information sur le client ${id} demandé par admin ${res.locals.oauth.token.userId}`)
    if(isNaN(parseInt(id))) {
        res.status(400).json({message: "invalid id type"})
    }
    let client = await CLIENT.findOne({
        where: {
            id: id
        },
        include: {
            model: ORGANISATION,
            as: "organisations",
            include: {
                model: ADMINISTRATEUR,
                as: 'administrateurs',
                where: {
                    id: res.locals.oauth.token.userId
                }
            }
        }
    })
    if (client === null) {
        res.status(404).json({message: "client not found"})
    }
    client = JSON.parse(JSON.stringify(client))
    try {
        delete client.organisations
    } catch(error) {}
    res.status(200).json(client)
}

exports.getRdvByClient = async (req, res) => {
    // #swagger.tags = ['Client']
    let id = req.params.id;
    LOGGER.info(`INFO - information sur le client ${id} et ses rdv demandé par admin ${res.locals.oauth.token.userId}`)
    if(isNaN(parseInt(id))) {
        res.status(400).json({message: "invalid id type"})
    }
    let client = await CLIENT.findOne({
        where: {
            id: id
        },
        include: [{
            model: ORGANISATION,
            as: "organisations",
            include: {
                model: ADMINISTRATEUR,
                as: 'administrateurs',
                where: {
                    id: res.locals.oauth.token.userId
                }
            }
        }, {
            model: RDV,
            as: "rdvs"
        }]
    })
    if (client === null) {
        res.status(404).json({message: "client not found"})
    }
    client = JSON.parse(JSON.stringify(client))
    try {
        delete client.organisations
    } catch(error) {}
    res.status(200).json(client)
}

exports.create = async (req, res) => {
    // #swagger.tags = ['Client']
    const {
        nom,
        prenom,
        email,
        telephone,
        age,
        organisation
    } = req.body;
    LOGGER.info(`INFO -création de client demandé par admin ${res.locals.oauth.token.userId} avec body : ${JSON.stringify({nom,prenom,email,telephone,age,organisation})}`)
    if (organisation === undefined) {
        res.status(400).json({message: "organisation not precised"})
    }
    let _organisation = await ORGANISATION.findOne({
        where: {
            id: organisation
        },
        include: {
            model: ADMINISTRATEUR,
            as: 'administrateurs'
        }
    })
    if (_organisation === null) {
        res.status(404).json({message: "organisation not found"})
    } else {
        _organisation = JSON.parse(JSON.stringify(_organisation))
        if (_organisation.administrateur !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur de l'organisation"})
        } else {
            await CLIENT.create({
                nom: nom,
                prenom: prenom,
                email: email,
                telephone: telephone,
                age: age,
                organisation: organisation
            })
            .then((client) => {
                LOGGER.info(`INFO - client ${client.id} créé`)
                res.json({
                    client,
                    message: 'client créé correctement'
                })
            })
            .catch((error) => {
                LOGGER.error(error)
                if (error.errors[0].type == "unique violation") {
                    res.status(400).json({message: "email is not unique"})
                } else {
                    res.status(400).json({message: error.errors[0].message})
                }
                
            });
        }
    }
}

exports.update = async (req, res) => {
    // #swagger.tags = ['Client']
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }
    LOGGER.info(`INFO - modification du client ${req.params.id} demandé par admin ${res.locals.oauth.token.userId}`)
    let client = await CLIENT.findOne({
        where: {
            id: id
        },
        include: {
            model: ORGANISATION,
            as: "organisations",
            include: {
                model: ADMINISTRATEUR,
                as: 'administrateurs',
                where: {
                    id: res.locals.oauth.token.userId
                }
            }
        }
    })
    if (client === null) {
        res.status(404).json({message: "client not found"})
    } else {
        if (client.organisations.administrateur !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur du client"})
        } else {
            await CLIENT.update(
                req.body, {
                    where: {
                        id: req.params.id
                    }
                }).then((client) => {
                    LOGGER.info(`INFO - modification du client ${req.params.id} effectué par admin ${res.locals.oauth.token.userId}`)
                    res.json({
                        client,
                        msg: 'client mis à jour'
                    })
                })
            .catch(res.status(500).send({
                message: "error with the json data"
            }))
        }
    }
    
}

exports.delete = async (req, res) => {
    // #swagger.tags = ['Client']
    LOGGER.info(`INFO - suppression du client ${req.params.id} demandé par admin ${res.locals.oauth.token.userId}`)
    let client = await CLIENT.findOne({
        where: {
            id: id
        },
        include: {
            model: ORGANISATION,
            as: "organisations",
            include: {
                model: ADMINISTRATEUR,
                as: 'administrateurs',
                where: {
                    id: res.locals.oauth.token.userId
                }
            }
        }
    })
    if (client === undefined) {
        res.status(404).json({message: "client not found"})
    } else {
        if (client.organisations.administrateur !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur du client"})
        } else {
            await CLIENT.destroy({
                where: {
                    id: req.params.id
                }
            })
                .then(() => {
                    LOGGER.info(`INFO - suppression du client ${req.params.id} effectué par admin ${res.locals.oauth.token.userId}`)
                    res.status(200).json({message: "client supprimé avec succès"})
                })
                .catch((error) => {LOGGER.error(error); res.status(500).json(error.errors[0].message)})
        }
    }
}
