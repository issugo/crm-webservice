//dépendances
const bcrypt = require('bcrypt');
const crypto = require('crypto')
const ADMINISTRATEUR = require("../models").administrateur;
const LOGGER = require('../config/winston.config')("administrateur");
const mail = require('../config/mail.config');

//fonctions pour les administrateurs
exports.getAll = async (req, res) => {
    // #swagger.tags = ['Administrateurs']
    LOGGER.info(`INFO - tout les admins demandés par ${res.locals.oauth.token.userId}`);
    ADMINISTRATEUR.count()
    .then(async (nbAdministrateur) => {
        let page = parseInt(req.query.page) - 1
        if (isNaN(page)) {
            page = 0
        }
        let administrateurs = await ADMINISTRATEUR.findAll({
            attributes: ['id', 'nom', 'prenom', 'email'],
            limit: 5,
            offset: 5 * page
        });
        res.status(200).json({administrateurs, pageTotale: Math.round(nbAdministrateur / 5) + 1})
    })
}

exports.getById = async (req, res) => {
    // #swagger.tags = ['Administrateurs']
    let id = req.params.id;
    if(isNaN(parseInt(id))) {
        res.status(400).json({message: "invalid id type"})
    }
    let administrateur = await ADMINISTRATEUR.findOne({
        attributes: ['id', 'nom', 'prenom', 'email'],
        where: {
            id: id
        }
    })
    if (administrateur === null) {
        res.status(404).json({message: "administrateur not found"})
    }
    LOGGER.info(`INFO - information sur l'admin ${id} demandé par admin ${res.locals.oauth.token.userId}`)
    res.status(200).json(administrateur)
}

exports.create = async (req, res) => {
    // #swagger.tags = ['Administrateurs']
    const {
        nom,
        prenom,
        email
    } = req.body;
    LOGGER.info(`INFO - création de l'admin demandé par admin ${res.locals.oauth.token.userId} avec body : ${JSON.stringify({nom,prenom,email})}`)
    const saltRounds = 10;
    const token = crypto.randomUUID();
    const hashedToken = await bcrypt.hash(token, saltRounds);
    await ADMINISTRATEUR.create({
            nom: nom,
            prenom: prenom,
            email: email,
            password: hashedToken
        })
        .then((administrateur) => {
            LOGGER.info(`INFO - admin ${administrateur.id} créé`)
            mail({
                from: 'legauf.services@gmail.com',
                to: administrateur.email,
                subject: 'Welcome to Legauf !',
                html: `<p>Hi ${administrateur.nom} ${administrateur.prenom}, welcome to Legauf crm-webservices.<br/><br/>api key : ${token}<br/><br/>We hope you will enjoy your stay. <br/><br/>Best regards </p><a href="https://crm-webservice.readthedocs.io/en/latest/">Link</a> of our documentation`
            })
            LOGGER.info(`INFO - mail envoyé à l'admin ${administrateur.id}`)
            res.status(200).json(administrateur)
        })
        .catch((error) => {
            LOGGER.error(error)
            if (error.errors[0].type == "unique violation") {
                res.status(400).json({message: "email is not unique"})
            } else {
                res.status(400).json({message: error.errors[0].message})
            }
            
        });
}

exports.update = async (req, res) => {
    // #swagger.tags = ['Administrateurs']
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }
    LOGGER.info(`INFO - modification de l'admin ${req.params.id} demandé par admin ${res.locals.oauth.token.userId}`)
    await ADMINISTRATEUR.update(
            req.body, {
                where: {
                    id: req.params.id
                }
            }).then(administrateur => {
                LOGGER.info(`INFO - ${administrateur.id} a été mis à jour`)
                res.json({
                    administrateur,
                    msg: 'administrateur mis à jour'
                })
            })
        .catch(res.status(400).send({
            message: "error with the json data"
        }))
}

exports.delete = async (req, res) => {
    // #swagger.tags = ['Administrateurs']
    await ADMINISTRATEUR.destroy({
        where: {
            id: req.params.id
        }
    })
        .then(() => {LOGGER.info(`INFO - ${req.params.id} admin a été détruit`); res.status(200).json({message: "administrateur supprimé avec succès"})})
        .catch((error) => {LOGGER.error(error); res.status(500).json(error.errors[0].message)})
}
