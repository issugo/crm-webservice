//dépendances
const PHOTO = require("../models").PHOTO;
const CLIENT = require("../models").client;
const ADMINISTRATEUR = require("../models").administrateur;
const fs = require('fs');
const LOGGER = require('../config/winston.config')("photo");

let photoName = 1;

//fonctions pour les PHOTOs
exports.getAll = async (req, res) => {
    // #swagger.tags = ['Photo']
    LOGGER.info(`INFO - tout les rdv demandés par ${res.locals.oauth.token.userId}`);
    PHOTO.count({
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
                
            }
        },
    }).then(async (nbPhoto) => {
        let page = parseInt(req.query.page) - 1
        if (isNaN(page)) {
            page = 0
        }
        let photos = await PHOTO.findAll({
            include: {
                model: CLIENT,
                as: "clients",
                include: {
                    model: ORGANISATION,
                    as: "organisations",
                    include: {
                        model: ADMINISTRATEUR,
                        as: 'administrateurs',
                        where: {
                            id: res.locals.oauth.token.userId
                        }
                    }
                    
                }
            },
            limit: 5,
            offset: 5 * page
        });
        let tempResult = []
        for(let i=0; i<photos.length; i++) {
            let temp = JSON.parse(JSON.stringify(photos[i]))
            try {
                delete temp.clients
            } catch(e) {}
            tempResult.push(temp)
        }
        res.status(200).json({photos: tempResult, pageTotale: nbPhoto})
    })
}

exports.getById = async (req, res) => {
    // #swagger.tags = ['Photo']
    let id = req.params.id;
    LOGGER.info(`INFO - information sur la photo ${id} demandé par admin ${res.locals.oauth.token.userId}`)
    if(isNaN(parseInt(id))) {
        res.status(400).json({message: "invalid id type"})
    }
    let photo = await PHOTO.findOne({
        where: {
            id: id
        },
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
                
            }
        },
    })
    if (photo === null) {
        res.status(404).json({message: "photo not found"})
    } else {
        photo = JSON.parse(JSON.stringify(photo))
        try {
            delete photo.clients
        } catch(error) {}
        res.status(200).json(photo)
    }
}

exports.create = async (req, res) => {
    // #swagger.tags = ['Photo']
    const {
        image,
        client
    } = req.body;
    LOGGER.info(`INFO - création de photo demandé par admin ${res.locals.oauth.token.userId}`)
    if (client === undefined) {
        res.status(400).json({message: "client not precised"})
    }
    await CLIENT.findOne({
        where: {id: client},
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
                
            }
        },
    }).then(async (client) => {
        if (client === null) {
            res.status(404).json({message: "client not found"})
        } else {
            if (client.organisations.administrateur !== req.userId) {
                res.status(403).json({message: "Vous n'êtes pas l'administrateur du client de l'organisation"})
            } else {
                path = `./public/images/${photoName}.png`;
                await PHOTO.create({
                    path: path,
                    date_envoi: Date.now(),
                    client: client
                })
                .then(photo => {
                    LOGGER.info(`INFO - photo ${photo.id} créé`)
                    fs.writeFile(path, image.split(',')[1], 'base64', function(err) {
                        LOGGER.error(`ERROR - ${path} image demandés par ${res.locals.oauth.token.userId} n'a aps pu être enregistré cause ${err}`)
                    });
                    res.json({
                        photo,
                        msg: 'PHOTO créé correctement'
                    })
                })
                .catch((error) => {
                    LOGGER.error(error)
                    res.status(400).json({message: error.errors[0].message})
                });
            }
        }
    })
    .catch((error) => {res.status(500).json(error.message)});

    
}

exports.update = async (req, res) => {
    // #swagger.tags = ['Photo']
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }
    LOGGER.info(`INFO - modification de la photo ${req.params.id} demandé par admin ${res.locals.oauth.token.userId}`)
    let photo = await PHOTO.findOne({
        where: {
            id: id
        },
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
            }
        }
    })
    if (photo === null) {
        res.status(404).json({message: "photo not found"})
    } else {
        if (photo.clients.organisations.administrateur !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur de la photo"})
        } else {
            await PHOTO.update(
                req.body, {
                    where: {
                        id: req.params.id
                    }
                }).then((photo) => {
                    LOGGER.info(`INFO - modification de la photo ${req.params.id} effectué par admin ${res.locals.oauth.token.userId}`)
                    res.json({
                        rdv,
                        msg: 'photo mis à jour'
                    })
                })
            .catch(res.status(500).send({
                message: "error with the json data"
            }))
        }
    }
}

exports.delete = async (req, res) => {
    // #swagger.tags = ['Photo']
    LOGGER.info(`INFO - suppression de la photo ${req.params.id} demandé par admin ${res.locals.oauth.token.userId}`)
    let photo = await PHOTO.findOne({
        where: {
            id: id
        },
        include: {
            model: CLIENT,
            as: "clients",
            include: {
                model: ORGANISATION,
                as: "organisations",
                include: {
                    model: ADMINISTRATEUR,
                    as: 'administrateurs',
                    where: {
                        id: res.locals.oauth.token.userId
                    }
                }
            }
        }
    })
    if (photo === undefined) {
        res.status(404).json({message: "photo not found"})
    } else {
        if (photo.clients.organisations.administrateur !== res.locals.oauth.token.userId) {
            res.status(403).json({message: "vous n'êtes pas l'administrateur de la photo"})
        } else {
            await PHOTO.destroy({
                where: {
                    id: req.params.id
                }
            })
                .then((photo) => {
                    fs.unlink(photo.path, (err) => {
                        if (err) {
                            LOGGER.error(err)
                        } else [
                            LOGGER.info(`INFO - suppression de la photo ${req.params.id} fini`)
                        ]
                    })
                    LOGGER.info(`INFO - suppression de la photo ${req.params.id} effectué par admin ${res.locals.oauth.token.userId}`)
                    res.status(200).json({message: "photo supprimée avec succès"})
                })
                .catch((error) => {LOGGER.error(error); res.status(500).json(error.errors[0].message)})
        }
    }
}

exports.show = async (req, res) => {
    let imageName = req.params.imageName;
    LOGGER.info(`INFO - photo ${imageName} demandé par admin ${res.locals.oauth.token.userId}`)
    if(isNaN(parseInt(id))) {
        res.status(400).json({message: "invalid id type"})
    } else {
        let photo = await PHOTO.findOne({
            where: {
                id: id
            },
            include: {
                model: CLIENT,
                as: "clients",
                include: {
                    model: ORGANISATION,
                    as: "organisations",
                    include: {
                        model: ADMINISTRATEUR,
                        as: 'administrateurs',
                        where: {
                            id: res.locals.oauth.token.userId
                        }
                    }
                    
                }
            },
        })
        if (photo === null) {
            res.status(404).json({message: "photo not found"})
        } else {
            res.sendFile(photo.path)
        }
    }
}
