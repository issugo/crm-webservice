const fs = require('fs');

exports.getDefault = (req, res) => {
    this.getInfo(req, res) 
}

exports.getInfo = async (req, res) => {
    const date = new Date()
    const filepath = `logs/info-${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}-${date.getHours()}.log`
    array = fs.readFileSync(filepath).toString().replace(/\t/g, "").replace(/\r/g, "").split("\n");
    res.json(array.slice(0,10))
}

exports.getError = async (req, res) => {
    const date = new Date()
    const filepath = `logs/error-${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}-${date.getHours()}.log`
    array = fs.readFileSync(filepath).toString().replace(/\t/g, "").replace(/\r/g, "").split("\n");
    res.json(array.slice(0,10))
}

exports.getSql = async (req, res) => {
    const date = new Date()
    const filepath = `logs/sql-${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}-${date.getHours()}.log`
    array = fs.readFileSync(filepath).toString().replace(/\t/g, "").replace(/\r/g, "").split("\n");
    res.json(array.slice(0,10))
}