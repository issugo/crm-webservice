//dépendances
const db = require("../models");
const Administrateur = db.administrateur;

checkDuplicateEmail = (req, res, next) => {
    Administrateur.findOne({
        where: {
            email: req.body.email
        }
    }).then(administrateur => {
        if (administrateur == null) {
            throw ''
        }
        res.status(400).send({
                message: "Failed! email is already in use!"
            });
        })
        .catch(() => {
            next()
            return
        });
};

const verifySignUp = {
    checkDuplicateEmail: checkDuplicateEmail
};

module.exports = verifySignUp;
