const fs = require('fs')

const make_log = (req, res, next) => {
    const date = new Date()
    const filename = date.getDate() + date.getMonth() + date.getFullYear() + ".log"
    const path = `./logs/${filename}`
    const log = '[' + date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear() + '] ' + res.locals.oauth.token.userId + ' admin made a request on ' + req.path + ' with verb ' + req.method + '\n\r'
    console.log("voila")
    fs.appendFile(path, log, (err) => {
        console.log(err)
    })
    console.log("voila2")
    next()
}

const logs = {
    make_log
}

module.exports = logs;