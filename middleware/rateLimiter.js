const rateLimit = require("express-rate-limit");

const rateLimiter = rateLimit({
  windowMs: 10 * 60 * 1000,
  max: 5,
  handler: function (req, res) {
    return res.status(429).json({
      error: 'You have exceeded the maximum number of requests in 10 min ! Please wait a while then try again'
    })
  }
});

module.exports = rateLimiter