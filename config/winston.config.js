const winston = require('winston');
require('winston-daily-rotate-file');

const logLevels = {
    error: 0,
    warn: 1,
    info: 2,
    verbose: 3,
    debug: 4,
    silly: 5,
    sql: 6
}

const defaultFormat = winston.format.combine(
    winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
    winston.format.align(),
    winston.format.printf(info => `${info.timestamp} ${info.level} ${info.component}: ${info.message}`)
);

const formatError = winston.format.combine(
    winston.format.errors({ stack: true }),
    winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
    winston.format.printf(info => `${info.timestamp} ${info.level} ${info.component}: ${info.message} ${info.stack}`),
    winston.format.align(),
);

const sqlFormat = winston.format.combine(
    winston.format.errors({ stack: true }),
    winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
    winston.format.printf(info => `${info.timestamp} ${info.level} ${info.component}: ${info.message}`),
    winston.format.align(),
);

const infoTransport = new winston.transports.DailyRotateFile({
    filename: './logs/info-%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    level: 'info',
    json: true,
    format: defaultFormat
});

const errorTransport = new winston.transports.DailyRotateFile({
    filename: './logs/error-%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    level: "error",
    json: true,
    format: formatError
});

const sqlTransport = new winston.transports.DailyRotateFile({
    filename: './logs/sql-%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    level: "sql",
    json: true,
    format: sqlFormat
});

const LoggerManager = (moduleName) => winston.createLogger({
    levels: logLevels,
    defaultMeta: { component: moduleName },
    transports: [
        infoTransport,
        errorTransport,
        sqlTransport
    ]
});

module.exports = LoggerManager;