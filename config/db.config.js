require('dotenv').config()

let host = process.env.DB_HOST || "localhost"
let user = process.env.DB_USER || "root"
let password = process.env.DB_PASSWORD || ""
let db = process.env.DB_DB
let dialect = process.env.DB_DIALECT || "mysql"

module.exports = {
    HOST: host,
    USER: user,
    PASSWORD: password,
    DB: db,
    dialect: dialect,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};