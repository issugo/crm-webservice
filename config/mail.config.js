const nodemailer = require('nodemailer');
const LoggerManager = require('./winston.config');

let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS
    }
});
let sendMail = (mailOptions)=>{
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            LoggerManager.error(error)
        } else {
          console.log('Email sent: ' + info.response);
        }
    });
};

module.exports = sendMail;