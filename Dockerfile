FROM node:14.18

WORKDIR /usr/app
COPY package.json .
RUN npm install
COPY . .

ENTRYPOINT [ "npm", "start" ]
