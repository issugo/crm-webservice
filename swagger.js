const swaggerAutogen = require('swagger-autogen')()

const outputFile = './swagger_output.json'
const endpointsFiles = ['routes/administrateur.routes.js', 'routes/auth.routes.js', 'routes/client.routes.js', 'routes/organisation.routes.js', 'routes/photo.routes.js', 'routes/rdv.routes.js']

const doc = {
    info: {
        version: "1.0.0",
        title: "LEGAUF API REST",
        description: "REST API created by LEGAUF for webservices project !!!"
    },
    host: "localhost:3000",
    basePath: "/",
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
}

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('./server.js')
})